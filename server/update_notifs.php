<?php
/**
 * This script updates the status of a game on the central server
 * (= needs for players or not)
 *
 * Usage: send to this page, in POST :
 * > POST[siteAlias] = the alias of the concerned site, as set in the central server
					  (see the file config.json)
 * > POST[action] = "askForPlayers" if a game is pending
 *               or "setGameComplete" if no more players are needed
 */

// This file contains the list of games which need players
$fileWaitingForPlayers = 'data/waitingForPlayers.json';

// Access the "body" part of the HTTP request
// Used for getting form sent by JS
$post = json_decode(file_get_contents('php://input'), true);

// TODO: check that the keys exist in the array
$action 	= filter_var($post['action'], FILTER_SANITIZE_STRING);
$siteAlias 	= filter_var($post['siteAlias'], FILTER_SANITIZE_STRING);

$waitingForPlayers = json_decode(file_get_contents($fileWaitingForPlayers), true);

// Updates the amount of players needed for the given game
if($action === 'askForPlayers') {
	$newAmount = 1;
}
elseif($action === 'setGameComplete') {
	$newAmount = 0;
}
else {
	echo 'Unknown action "'.$action.'"';
	exit;
}

// Writes the new data in the JSON file
$waitingForPlayers[$siteAlias] = $newAmount;
file_put_contents($fileWaitingForPlayers, json_encode($waitingForPlayers));

// Show API result
echo json_encode([]);
?>
