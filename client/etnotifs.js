function apiServerRoot() {
	
	//return "http://etnotifs/server/";
	//return "http://eternaltwin.nadazone.fr/etnotifs/";
	return "";
}


/**
 * Sends a form with the GET or POST method
 * 
 * @param {string} method  The HTTP method to send the data (GET or POST)
 * @param {string} params  The additional parameters to send to the API, 
  *                        as a unique string for a GET request
 *                         E.g.: "action=get&citizen_id=87"
 */
 async function callApi(apiUrl, method, params) {
    
    let option = {
            method: method,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };
    if(method==="GET") {
        apiUrl += "?"+params;
    } else {
        option.body = JSON.stringify(params);
    }
    
    // For debugging : uncomment this line to watch in the browser console 
    // how many times you call the distant APIs, and eventually optimize the redundant calls
//    console.log("API call: "+apiUrl);
    
    return await fetch(apiUrl, option).then(toJson);
}


/**
 * Converts a string to JSON and prints the malformed JSON in the console
 */
async function toJson(apiResult) {
    
    try {
        //.text() pour retourner un texte brut, ou .json() pour parser du JSON
        return await apiResult.clone().json();
    } catch (e) {
        await apiResult.clone().text().then(apiResult=>{
            console.error(e);
            console.groupCollapsed("See the result returned by the API:");
            console.log(apiResult);
            console.groupEnd();
            throw e;
        });
    }
}


/**
 * Afficher/masquer l'élement indiqué en cliquant sur un lien
 * 
 * @param {string} elementId     L'id HTML de l'élément à afficher/masquer
 */
function toggle(elementId) {

    // getComputedStyle(...) récupère la propriété en tenant compte de la CSS.
    // Un simple getElementById(...).style ne tiendrait compte que des
    // styles en ligne dans le HTML.
    var currentDisplay = window.getComputedStyle(document.getElementById(elementId)).display;
        
    document.getElementById(elementId).style.display = (currentDisplay === "none") ? "block" : "none";
}


function htmlNotif(siteName, siteUrl) {
	
	return "<div class=\"notif\">\
			Une partie vous attend sur <strong>"+siteName+"</strong> !<br>\
			<a href=\""+siteUrl+"\" class=\"joinButton\">Rejoindre la partie</a>\
		</div>";
}


function htmlAskForPlayers(siteAlias) {
	
	return '<button onclick="actionInvitePlayers(\''+siteAlias+'\', \'askForPlayers\')">&#x1F465; Demander des joueurs<br>pour '+siteAlias+'</button>';
}


function htmlSetGameComplete(siteAlias) {
	
	return '<button onclick="actionInvitePlayers(\''+siteAlias+'\', \'setGameComplete\')">	&#x1F6D1; Arrêter la demande de joueurs<br>pour '+siteAlias+'</button>';
}


/**
 * Displays the number of notifications in the red dot 
 * @param {int} nbrNotifs The number of notications
 */
function htmlNbrNotifs(nbrNotifs) {
	
	return (nbrNotifs > 0) ? '<span class="dotnum">'+nbrNotifs+'</span>' : '';
}


/**
 * Switches the buttons "Ask for players"/"The game is complete"
 * @param {string} siteAlias The alias of the current site (directquiz, majority...)
 * @param {int} nbrPlayersNeeded Number of players waiting for a game 
								 in the current site
 */
function setInvitationButton(siteAlias, nbrPlayersNeeded) {
	
	document.querySelector("#etNotifs #playersInvitation").innerHTML =
		(nbrPlayersNeeded===0) ? htmlAskForPlayers(siteAlias) : htmlSetGameComplete(siteAlias);
}


/**
 * Displays the list of notifications (= the games waiting for players)
 * @param {object} json The JSON returned by the notifs API
 */
function setNotifsList(json) {
	
	var textNotif = "";
	
	for(let siteAlias in json.waitingForPlayers) {
		let siteCaracs = json.waitingForPlayers[siteAlias];
		
		if(siteCaracs.nbrPlayersNeeded >= 1) {
			textNotif += htmlNotif(siteCaracs.siteName, siteCaracs.siteUrl);
		}
	}
	
	document.querySelector("#etNotifs #notifs").innerHTML = textNotif;
}


/**
 * Displays the number of notifications in the red dot
 * @param {object} json The JSON returned by the notifs API
 */
function setNbrNotifs(json) {
	
	var nbrGamesWaiting = 0;
	
	for(let siteAlias in json.waitingForPlayers) {		
		if(json.waitingForPlayers[siteAlias].nbrPlayersNeeded >= 1) {
			nbrGamesWaiting++;
		}
	}
	
	document.querySelector("#etNotifs #nbrNotifs").innerHTML = htmlNbrNotifs(nbrGamesWaiting);
}


/**
 * Sends data to the API to ask for new players or not 
 * @param {string} siteAlias
 * @param {string} "askForPlayers" to invite new players
 *				   "setGameComplete" if the game doesn't need players anymore
 */
async function actionInvitePlayers(siteAlias, action) {
	
	let json = await callApi(apiServerRoot()+"server/update_notifs.php", "POST", 
							 {"action":action, "siteAlias":siteAlias});
			
	// TODO: we should not call the API to update the notifications list
	// (we already know what we have sent)
	actionUpdateNotifs(siteAlias);
}


/**
 * Main function: updates everything relative to the notifications
 * (contents of the notifications, red dot...)
 */
async function actionUpdateNotifs(siteAlias) {
	
	let json = await callApi(apiServerRoot()+"server/api.php", "GET", "");

	// Shows the list of games waiting for players
	setNotifsList(json);
	
	// Shows the number of notifications (red dot)
	setNbrNotifs(json);
	
	// If we have clicked on the button "Ask for players", switch to 
	// the button "Set game complete" (and reciprocally)
	setInvitationButton(siteAlias, json.waitingForPlayers[siteAlias].nbrPlayersNeeded);
}


actionUpdateNotifs("directquiz");
